<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class MediasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 5;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('medias')->insert([ //,
                'name' => $faker->unique()->name,
                'type' => $faker->numberBetween(1,3),
                'link' => $faker->imageUrl($width = 640, $height = 480),
                'concert_id' => $faker->numberBetween(1,10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
