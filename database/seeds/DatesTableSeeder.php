<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class DatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 10;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('dates')->insert([ //,
                'date' => \Carbon\Carbon::now(),
                'prix' => $faker->numberBetween(50,100),
                'concert_id' => $faker->numberBetween(1,10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
