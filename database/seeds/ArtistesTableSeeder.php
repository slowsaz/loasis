<?php

use Illuminate\Database\Seeder;

class ArtistesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 33;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('artistes')->insert([ //,
                'name' => $faker->unique()->name,
                'biography' => $faker->text($maxNbChars = 191),
                'logo' => $faker->imageUrl($width = 640, $height = 480),
                'instagram' => $faker->url,
                'youtube' => $faker->url,
                'bandcamp' => $faker->url,
                'siteweb' => $faker->url,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
