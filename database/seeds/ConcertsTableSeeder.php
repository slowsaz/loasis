<?php

use Faker\Factory;
use Illuminate\Database\Seeder;

class ConcertsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 10;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('concerts')->insert([ //,
                'name' => $faker->unique()->name,
                'description' => $faker->text($maxNbChars = 191),
                'duree' => $faker->numberBetween(1,3),
                'categorie_id' => $faker->numberBetween(1,10),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
