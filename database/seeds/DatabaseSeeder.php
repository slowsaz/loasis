<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([UsersTableSeeder::class,ArtistesTableSeeder::class,ConcertsTableSeeder::class,CategoriesTableSeeder::class,TagsTableSeeder::class,MediasTableSeeder::class,ArtisteConcertTableSeeder::class,ArtisteTagTableSeeder::class]);
    }
}
