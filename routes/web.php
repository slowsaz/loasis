<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('concerts/categories/{id}', 'ConcertController@indexByCategory')->name('concertsCategories');
Route::get('artistes/tags/{id}', 'ArtisteController@indexByTag')->name('artistesTags');
Route::get('medias/concerts/{id}','MediaController@indexByConcert')->name('mediasConcerts');
Route::get('ajoutPaniers/{id}','DateController@ajoutPanier')->name('paniers');
Route::get('paniers/{id}','DateController@indexPanier')->name('panier');
Route::get('commandes/users/{id}','CommandeController@indexByUser')->name('commandesUsers');
Route::get('ligneCommande','CommandeController@ligneCommande')->name('ligneCommande');
Route::resource('artistes','ArtisteController');
Route::resource('concerts','ConcertController');
Route::resource('categories','CategorieController');
Route::resource('tags','TagController');
Route::resource('medias','MediaController');
Route::resource('dates','DateController');
Route::resource('commandes','CommandeController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
