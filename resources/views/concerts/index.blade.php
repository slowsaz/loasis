@extends ('layouts.master')

@section('title')
    <title>Home</title>
@stop

@section('header')

@stop
@section('content')
    <h1>Mes concerts</h1>
    <div class="row">
        @foreach($concerts as $concert)

                <div class="col-lg-4 mb-4 text-dark">
                    <div class="card h-100">
                        <h4 class="card-header">{{$concert->name}}</h4>
                        <div class="card-body">
                            <p class="card-text">{{$concert->description}}</p>
                        </div>
                        <div class="card-footer">
                            <a href="{{route('mediasConcerts',['concert'=>$concert->id])}}" class="btn btn-dark">Learn More</a>
                        </div>
                    </div>
                </div>


            <article class="col-sm-6 text-center border border-secondary rounded">
                <a href="{{"/concerts/".$concert->id}}">{{$concert->name}}</a>
                <p>{{$concert->description}}</p>
                <span>Durée du concert : {{$concert->duree}} heures</span>
                <span>Catégorie du concert :<a href="{{"/categories/".$concert->categorie->id}}">{{$concert->categorie->name}}</a></span>
                <br>
                Concerts :
                @foreach($concert->artistes as $artiste)
                    <a href="{{"/artistes/".$artiste->id}}">{{$artiste->name}}</a>
                @endforeach
                <br>
                <a href="{{ route('concerts.edit',['concert' => $concert->id]) }}" class="btn btn-secondary text-center w-25">Editer</a>
                <form action="{{ route('concerts.destroy', ['id' => $concert->id]) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger text-center w-25">DELETE</button>
                    </div>
                </form>
            </article>
        @endforeach
            {{$concerts -> links()}}
    </div>

@stop
