@extends ('layouts.master')

@section('title')
    <title>Create concert</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/concerts">Home</a></li>
        <li><a href="/concerts/1/edit">Update</a> </li>
        <li><a href="/concerts">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    {!! Form::open(['route' => 'concerts.store']) !!}
    <div class="col-lg-2">
        <div class="form-group">
            {{Form::label('name', 'Name :')}}
            {{Form::text('name', 'Name')}}
        </div>
        <div class="form-group">
            {{Form::label('description', 'Description :')}}
            {{Form::textarea('description', 'Description')}}
        </div>
        <div class="form-group">
            {{Form::label('duree', 'Durée :')}}
            {{Form::number('duree', 'Durée')}}
        </div>
        <div class="form-group">
            {{Form::label('artistes', 'Artistes :')}}
            {{Form::select('artistes[]', $artistes,null,['multiple' => 'multiple'])}}
        </div>
        <div class="form-group">
            {{Form::label('categorie_id', 'Catégorie :')}}
            {{Form::select('categorie_id', $categories)}}
        </div>
        <div class="form-group">
            {{Form::submit('Envoyer')}}
        </div>
    </div>
    {!! Form::close() !!}
    <div class="row">
        <article class="col-sm-3">

        </article>
    </div>
@stop
