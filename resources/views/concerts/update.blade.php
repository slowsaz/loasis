@extends ('layouts.master')

@section('title')
    <title>Update concert</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/concerts">Home</a></li>
        <li><a href="/concerts/create">Create</a></li>
        <li><a href="/concerts/2/edit">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    {!! Form::model($concert,['method' => 'put', 'route' => ['concerts.update',$concert->id]])!!}
    <div class="col-lg-2">
        <div class="form-group">
            {{Form::label('name', 'Name :')}}
            {{Form::text('name', null)}}
        </div>
        <div class="form-group">
            {{Form::label('description', 'Description :')}}
            {{Form::textarea('description', null)}}
        </div>
        <div class="form-group">
            {{Form::label('duree', 'Durée :')}}
            {{Form::number('duree', null)}}
        </div>
        <div class="form-group">
            {{Form::label('categorie_id', 'Catégorie :')}}
            {{Form::select('categorie_id', $categories,null)}}
        </div>
        <div class="form-group">
            {{Form::label('artistes', 'Artistes :')}}
            {{Form::select('artistes[]',$artistes, null ,['multiple' => 'multiple'])}}
        </div>
        <div class="form-group">
            {{Form::submit('Envoyer')}}
        </div>
    </div>
    {!! Form::close() !!}
    <div class="row">
        <article class="col-sm-3">

        </article>
    </div>
@stop
