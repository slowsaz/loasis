@extends ('layouts.master')

@section('title')
    <title>View concert {{$concert->id}}</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/concerts">Home</a></li>
        <li><a href="{{"/concerts/".$concert->id."/edit"}}">Edit</a></li>
        <li><a href="/concerts">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    <div class="row">
        <article class="col-sm-6 text-center border border-secondary rounded">
            <span>Voici le concert {{$concert->name}}</span>
            <p> {{$concert->description}}</p>
            Artistes :
            @foreach($concert->artistes as $artiste)
                <a href="{{"/artistes/".$artiste->id}}">{{$artiste->name}}</a>
            @endforeach
        </article>
    </div>
@stop
