@extends ('layouts.master')

@section('title')
    <title>View media {{$media->id}}</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/medias">Home</a></li>
        <li><a href="{{"/medias/".$media->id."/edit"}}">Edit</a></li>
        <li><a href="/medias">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    <div class="row">
        <article class="col-sm-6 text-center border border-secondary rounded">
            <span>{{$media->name}}</span>
            <span> {{$media->type}}</span>
            <iframe width="420" height="315"
                    src="{{$media->link}}">
            </iframe>
        </article>
    </div>
@stop
