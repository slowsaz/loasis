@extends ('layouts.master')

@section('title')
    <title>Create media</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/medias">Home</a></li>
        <li><a href="/medias/1/edit">Update</a> </li>
        <li><a href="/medias">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    {!! Form::open(['route' => 'medias.store']) !!}
    <div class="col-lg-2">
        <div class="form-group">
            {{Form::label('name', 'Name :')}}
            {{Form::text('name', 'Name')}}
        </div>
        <div class="form-group">
            {{Form::label('link', 'Lien :')}}
            {{Form::text('link', null)}}
        </div>
        <div class="form-group">
            {{Form::label('type', 'Type :')}}
            {{Form::number('type', 'Type')}}
        </div>
        <div class="form-group">
            {{Form::label('concert_id', 'Concert :')}}
            {{Form::select('concert_id', $concerts)}}
        </div>
        <div class="form-group">
            {{Form::submit('Envoyer')}}
        </div>
    </div>
    {!! Form::close() !!}
    <div class="row">
        <article class="col-sm-3">

        </article>
    </div>
@stop
