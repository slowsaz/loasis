@extends ('layouts.master')

@section('title')
    <title>Home</title>
@stop

@section('header')

@stop
@section('content')
    <h1>Mes médias</h1>
    <div class="row">
        @foreach($medias as $media)

                <div class="col-lg-4 mb-4 text-dark">
                    <div class="card h-100">
                        <h4 class="card-header">{{$media->name}}</h4>
                        <div class="card-body">
                            <p class="card-text"><video width="400"><source src="{{$media->link}}"></video></p>
                        </div>
                        <div class="card-footer">
                            <a href="{{route('medias.show',['media'=>$media->id])}}" class="btn btn-dark">Learn More</a>
                        </div>
                    </div>
                </div>


            <article class="col-sm-6 text-center border border-secondary rounded">
                <iframe width="450" height="315" src="{{$media->link}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <span>Type du media : {{$media->type}}</span>
                <span>Concert lié au media : <a href="{{"/concerts/".$media->concert->id}}">{{$media->concert->name}}</a></span>
                <a href="{{ route('medias.edit',['media' => $media->id]) }}" class="btn btn-secondary text-center w-25">Editer</a>
                <form action="{{ route('medias.destroy', ['id' => $media->id]) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger text-center w-25">DELETE</button>
                    </div>
                </form>
            </article>
        @endforeach
    </div>

@stop
