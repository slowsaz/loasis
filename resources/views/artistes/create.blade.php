@extends ('layouts.master')

@section('title')
    <title>Create artiste</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/artistes">Home</a></li>
        <li><a href="/artistes/edit/1">Update</a> </li>
        <li><a href="/artistes">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    {!! Form::open(['route' => 'artistes.store','files'=>true]) !!}
    <div class="col-lg-2">
        <div class="form-group">
            {{Form::label('name', 'Name :')}}
            {{Form::text('name', 'Name')}}
        </div>
        <div class="form-group">
            {{Form::label('biography', 'Description :')}}
            {{Form::textarea('biography', 'Description')}}
        </div>
        <div class="form-group">
            {{Form::label('instagram', 'Instagram :')}}
            {{Form::text('instagram', 'Instagram')}}
        </div>
        <div class="form-group">
            {{Form::label('youtube', 'Youtube :')}}
            {{Form::text('youtube', 'Youtube')}}
        </div>
        <div class="form-group">
            {{Form::label('bandcamp', 'Bandcamp :')}}
            {{Form::text('bandcamp', 'Bandcamp')}}
        </div>
        <div class="form-group">
            {{Form::label('siteweb', 'Site :')}}
            {{Form::text('siteweb', 'Site')}}
        </div>
        <div class="form-group">
            {{Form::label('tags', 'Tag :')}}
            {{Form::select('tags[]', $tags,null,['multiple' => 'multiple'])}}
        </div>
        <div class="form-group">
            {{Form::label('concerts', 'Concert :')}}
            {{Form::select('concerts[]', $concerts,null,['multiple' => 'multiple'])}}
        </div>
        <div class="form-group">
            {{Form::label('logo', 'Lien image :  ')}}
            {{Form::file('logo', null)}}
        </div>
        <div class="form-group">
            {{Form::submit('Envoyer')}}
        </div>
    </div>
    {!! Form::close() !!}
    <div class="row">
        <article class="col-sm-3">

        </article>
    </div>
@stop
