@extends ('layouts.master')

@section('title')
    <title>Update artiste</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/artistes">Home</a></li>
        <li><a href="/artistes/create">Create</a></li>
        <li><a href="/artistes/2/edit">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    {!! Form::model($artiste,['method' => 'put', 'route' => ['artistes.update',$artiste->id],'files'=>true])!!}
    <div class="col-lg-2">
        <div class="form-group">
            {{Form::label('name', 'Name :')}}
            {{Form::text('name', null)}}
        </div>
        <div class="form-group">
            {{Form::label('biography', 'Description :')}}
            {{Form::textarea('biography', null)}}
        </div>
        <div class="form-group">
            {{Form::label('instagram', 'Instagram :')}}
            {{Form::text('instagram', null)}}
        </div>
        <div class="form-group">
            {{Form::label('youtube', 'Youtube :')}}
            {{Form::text('youtube', null)}}
        </div>
        <div class="form-group">
            {{Form::label('bandcamp', 'Bandcamp :')}}
            {{Form::text('bandcamp', null)}}
        </div>
        <div class="form-group">
            {{Form::label('siteweb', 'Site :')}}
            {{Form::text('siteweb', null)}}
        </div>
        <div class="form-group">
            {{Form::label('logo', 'Lien image :  ')}}
            {{Form::file('logo', null)}}
        </div>
        <div class="form-group">
            {{Form::label('tags', 'Tag :')}}
            {{Form::select('tags[]', $tags,null,['multiple' => 'multiple'])}}
        </div>
        <div class="form-group">
            {{Form::label('concerts', 'Concert :')}}
            {{Form::select('concerts[]', $concerts,null,['multiple' => 'multiple'])}}
        </div>
        <div class="form-group">
            {{Form::submit('Envoyer')}}
        </div>
    </div>
    {!! Form::close() !!}
    <div class="row">
        <article class="col-sm-3">

        </article>
    </div>
@stop
