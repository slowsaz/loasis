@extends ('layouts.master')

@section('title')
    <title>Artistes</title>
@stop

@section('header')

@stop
@section('content')
    <h1>Mes artistes</h1>
    <div class="row">
        @foreach($artistes as $artiste)

                <div class="col-lg-4 mb-4 text-dark">
                    <div class="card h-100">
                        <h4 class="card-header">{{$artiste->name}}</h4>
                        <div class="card-body">
                            <p class="card-text">{{$artiste->biography}}</p>
                        </div>
                        <div class="card-footer">
                            <a href="{{route('artistes.show',['artiste'=>$artiste->id])}}" class="btn btn-dark">Learn More</a>
                        </div>
                    </div>
                </div>


            <article class="col-sm-6 text-center border border-secondary rounded">
                <a href="{{"/artistes/".$artiste->id}}"><img src=" {{$artiste->logo}}" alt="{{$artiste->name}}"></a>
                <p>{{$artiste->biography}}</p>
                <ul class="nav navbar-dark">
                    <li><i class="fab fa-instagram"><a href="{{$artiste->instagram}}">Instagram</a></i></li>
                    <li><i class="fab fa-youtube"><a href="{{$artiste->youtube}}">Youtube</a></i></li>
                    <li><i class="fab fa-bandcamp"><a href="{{$artiste->bandcamp}}">Bandcamp</a></i></li>
                    <li><i class="fab fa-firefox"><a href="{{$artiste->siteweb}}">Site Web</a></i></li>
                </ul>
                @foreach($artiste->concerts as $concert)
                    <span>Concert :<a href="{{"/concerts/".$concert->id}}">{{$concert->name}}</a></span>
                @endforeach
                @foreach($artiste->tags as $tag)
                    <span>Tag :<a href="{{"/tags/".$tag->id}}">{{$tag->name}}</a></span>
                @endforeach
                <a href="{{ route('artistes.edit',['artiste' => $artiste->id]) }}" class="btn btn-secondary text-center w-25">Editer</a>
                <form action="{{ route('artistes.destroy', ['id' => $artiste->id]) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger text-center w-25">DELETE</button>
                    </div>
                </form>
            </article>
        @endforeach
            {{$artistes -> links()}}
    </div>

@stop
