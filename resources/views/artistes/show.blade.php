@extends ('layouts.master')

@section('title')
    <title>View artiste {{$artiste->id}}</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/artistes">Home</a></li>
        <li><a href="{{"/artistes/".$artiste->id."/edit"}}">Edit</a></li>
        <li><a href="/artistes">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    <div class="row">
        <article class="col-sm-6 text-center border border-secondary rounded">
            <p>Voici l'artiste {{$artiste->name}}</p>
            <a href="{{"/artistes/".$artiste->id}}"><img src=" {{$artiste->logo}}" alt="{{$artiste->name}}"></a>
            <p> {{$artiste->biography}}</p>
            @foreach($artiste->concerts as $concert)
                <a href="{{"/concerts/".$concert->id}}">{{$concert->name}}</a>
            @endforeach
            @foreach($artiste->tags as $tag)
                <span><a href="{{"/tags/".$tag->id}}">{{$tag->name}}</a></span>
            @endforeach
        </article>
    </div>
@stop
