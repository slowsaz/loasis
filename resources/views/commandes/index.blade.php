@extends ('layouts.master')

@section('title')
    <title>Home</title>
@stop

@section('header')

@stop
@section('content')
    <h1>Mes commandes</h1>
    <div class="row">
        @foreach($commandes as $commande)

                <div class="col-lg-4 mb-4 text-dark">
                    <div class="card h-100">
                        <h4 class="card-header"><a class="text-dark" href={{"/commandes/".$commande->id}}>{{$commande->id}}</a></h4>
                        <div class="card-body">
                            <p class="card-text">Prix : {{$commande->prixTotal}} €</p>
                        </div>
                        <div class="card-footer">
                            <a href="{{route('commandes.show',['commande'=>$commande->id])}}" class="btn btn-dark">Learn More</a>
                        </div>
                    </div>
                </div>


            <article class="col-sm-6 text-center border border-secondary rounded">
                <a href="{{"/users/".$commande->user_id}}">Numéro client : {{$commande->user_id}}</a>
                <span>Status du paiement : {{$commande->status}}</span>
                <span>Type du paiement : {{$commande->paiement}}</span>
                {{--@foreach($commande->concerts as $concert)--}}
                    {{--<span><a href="{{"/concerts/".$concert->id}}">{{$concert->name}}</a></span>--}}
                {{--@endforeach--}}
                {{--@foreach($commande->tags as $tag)--}}
                    {{--<span><a href="{{"/tags/".$tag->id}}">{{$tag->name}}</a></span>--}}
                {{--@endforeach--}}
                <a href="{{ route('commandes.edit',['commande' => $commande->id]) }}" class="btn btn-secondary text-center w-25">Editer</a>
                <form action="{{ route('commandes.destroy', ['id' => $commande->id]) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger text-center w-25">DELETE</button>
                    </div>
                </form>
            </article>
        @endforeach
    </div>

@stop
