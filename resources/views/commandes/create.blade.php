@extends ('layouts.master')

@section('title')
    <title>Create commande</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/commandes">Home</a></li>
        <li><a href="/commandes/edit/1">Update</a> </li>
        <li><a href="/commandes">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    {!! Form::open(['route' => 'commandes.store'],$user,$prix) !!}
    <div class="col-lg-2">
        <div class="form-group">
            {{Form::label('paiement', 'Type de paiement :')}}
            {{Form::text('paiement', 'Paiement')}}
        </div>
        <div class="form-group">
            {{Form::label('status', 'Status de la commande :')}}
            {{Form::text('status', 'Status')}}
        </div>
        <div class="form-group">
            {{Form::label('prixTotal', 'Prix :')}}
            {{Form::number('prixTotal', $prix)}}
        </div>
        <div class="form-group">
            {{Form::hidden('user_id', $user)}}
        </div>
        <div class="form-group">
            {{Form::submit('Envoyer')}}
        </div>
    </div>
    {!! Form::close() !!}
    <div class="row">
        <article class="col-sm-3">

        </article>
    </div>
@stop
