@extends ('layouts.master')

@section('title')
    <title>View commande {{$commande->id}}</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/commandes">Home</a></li>
        <li><a href="{{"/commandes/".$commande->id."/edit"}}">Edit</a></li>
        <li><a href="/commandes">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    <div class="row">
        <article class="col-sm-6 text-center border border-secondary rounded">
            <a href="{{"/commandes/".$commande->id}}">Voici la commande {{$commande->id}}</a>
            <span>Type de paiement : {{$commande->paiement}}</span>
            <span>Status de la commande : {{$commande->status}}</span>
            <span>Prix total : {{$commande->prixTotal}}</span>
            <span>La commande appartient à l'utilisateur {{$commande->user_id}}</span>
            {{--@foreach($commande->concerts as $concert)--}}
                {{--<a href="{{"/concerts/".$concert->id}}">{{$concert->name}}</a>--}}
            {{--@endforeach--}}
            {{--@foreach($commande->tags as $tag)--}}
                {{--<a href="{{"/tags/".$tag->id}}">{{$tag->name}}</a>--}}
            {{--@endforeach--}}
        </article>
    </div>
@stop
