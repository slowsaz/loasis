@extends ('layouts.master')

@section('title')
    <title>Home</title>
@stop

@section('header')

@stop
@section('content')
    <h1>Mes tags</h1>
    <div class="row">
        @foreach($tags as $tag)

                <div class="col-lg-4 mb-4 text-dark">
                    <div class="card h-100">
                        <h4 class="card-header">{{$tag->name}}</h4>
                        <div class="card-body">
                            @foreach($tag->artistes as $artiste)
                                <a class="text-dark"href="{{"/artistes/".$artiste->id}}">{{$artiste->name}}</a>
                            @endforeach
                            <p class="card-text">{{$tag->biography}}</p>
                        </div>
                        <div class="card-footer">
                            <a href="{{route('artistesTags',['tag'=>$tag->id])}}" class="btn btn-dark">Learn More</a>
                        </div>
                    </div>
                </div>


            <article class="col-sm-6 text-center border border-secondary rounded">
                <a href="{{"/tags/".$tag->id}}">{{$tag->name}}</a>
                <span><p>{{$tag->description}}</p></span>
                <a href="{{ route('tags.edit',['tag' => $tag->id]) }}" class="btn btn-secondary text-center w-25">Editer</a>
                <form action="{{ route('tags.destroy', ['id' => $tag->id]) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger text-center w-25">DELETE</button>
                    </div>
                </form>
            </article>
        @endforeach
    </div>

@stop
