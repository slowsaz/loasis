@extends ('layouts.master')

@section('title')
    <title>View tag {{$tag->id}}</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/tags">Home</a></li>
        <li><a href="{{"/tags/".$tag->id."/edit"}}">Edit</a></li>
        <li><a href="/tags">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    <div class="row">
        <article class="col-sm-6 text-center border border-secondary rounded">
            <p>Voici le tag {{$tag->name}}</p>
            @foreach($tag->artistes as $artiste)
                <a href="{{"/artistes/".$artiste->id}}">{{$artiste->name}}</a>
            @endforeach
            <p> {{$tag->description}}</p>
        </article>
    </div>
@stop
