@extends ('layouts.master')

@section('title')
    <title>Home</title>
@stop

@section('header')

@stop
@section('content')
    <h1>Mes dates</h1>
    <div class="row">
        @foreach($dates as $date)

                <div class="col-lg-4 mb-4 text-dark">
                    <div class="card h-100">
                        <h4 class="card-header">{{$date->date}}</h4>
                        <div class="card-body">
                            <p class="card-text">{{$date->prix}} euros</p>
                        </div>
                        <div class="card-footer">
                            <a href="{{route('dates.show',['date'=>$date->id])}}" class="btn btn-dark">Learn More</a>
                        </div>
                    </div>
                </div>


            <article class="col-sm-6 text-center border border-secondary rounded">
                <a href="{{"/dates/".$date->id}}"></a>
                <p>{{$date->prix}} euros</p>
                <span><a href="{{"/concerts/".$date->concert->id}}">{{$date->concert->name}}</a></span>
                {{--@foreach($date->tags as $tag)--}}
                    {{--<span><a href="{{"/tags/".$tag->id}}">{{$tag->name}}</a></span>--}}
                {{--@endforeach--}}
                <span>
                    <a href="{{ route('paniers',['date' => $date->id,$quantite=5]) }}" class="btn btn-light text-center w-25">Ajouter</a>
                </span>
                    <a href="{{ route('dates.edit',['date' => $date->id]) }}" class="btn btn-secondary text-center w-25">Editer</a>
                <form action="{{ route('dates.destroy', ['id' => $date->id]) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger text-center w-25">DELETE</button>
                    </div>
                </form>
            </article>
        @endforeach
            {{$dates -> links()}}
    </div>

@stop
