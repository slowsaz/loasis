@extends ('layouts.master')

@section('title')
    <title>Create date</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/dates">Home</a></li>
        <li><a href="/dates/edit/1">Update</a> </li>
        <li><a href="/dates">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    {!! Form::open(['route' => 'dates.store']) !!}
    <div class="col-lg-2">
        <div class="form-group">
            {{Form::label('date', 'Date :')}}
            {{Form::date('date',  \Carbon\Carbon::now())}}
        </div>
        <div class="form-group">
            {{Form::label('prix', 'Prix :')}}
            {{Form::number('prix', 0)}}
        </div>
        <div class="form-group">
            {{Form::label('concert_id', 'Concert :')}}
            {{Form::select('concert_id', $concert)}}
        </div>
        <div class="form-group">
            {{Form::submit('Envoyer')}}
        </div>
    </div>
    {!! Form::close() !!}
    <div class="row">
        <article class="col-sm-3">

        </article>
    </div>
@stop
