@extends ('layouts.master')

@section('title')
    <title>View date {{$date->id}}</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/dates">Home</a></li>
        <li><a href="{{"/dates/".$date->id."/edit"}}">Edit</a></li>
        <li><a href="/dates">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    <div class="row">
        <article class="col-sm-6 text-center border border-secondary rounded">
            <p>Voici la date {{$date->date}}</p>
            <p> {{$date->prix}}</p>
            {{--@foreach($date->concerts as $concert)--}}
                {{--<a href="{{"/concerts/".$concert->id}}">{{$concert->name}}</a>--}}
            {{--@endforeach--}}
            {{--@foreach($date->tags as $tag)--}}
                {{--<a href="{{"/tags/".$tag->id}}">{{$tag->name}}</a>--}}
            {{--@endforeach--}}
        </article>
    </div>
@stop
