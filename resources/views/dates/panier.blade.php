@extends ('layouts.master')

@section('title')
    <title>Home</title>
@stop

@section('header')

@stop
@section('content')
    <h1>Mon panier</h1>
    <div class="row">
        @foreach($paniers as $panier)

            <div class="col-lg-4 mb-4 text-dark">
                <div class="card h-100">
                    <h4 class="card-header">{{$panier->date}}</h4>
                    <div class="card-body">
                        <p class="card-text">{{$panier->concert->name}}</p>
                        <p class="card-text">{{$panier->prix}} euros</p>
                        <p class="card-text">{{$panier->pivot_user_id}}</p>
                        <p class="card-text">{{$panier->pivot_quantite}}</p>
                    </div>
                    <div class="card-footer">
                        <a href="{{route('dates.show',['date'=>$panier->id])}}" class="btn btn-dark">Learn More</a>
                    </div>
                </div>
            </div>


            <article class="col-sm-6 text-center border border-secondary rounded">
                <a href="{{"/concerts/".$panier->concert->id}}">{{$panier->concert->name}}</a>
                <p>{{$panier->prix}} euros</p>
                <span><a href="{{"/concerts/".$panier->id}}">{{$panier->date}}</a></span>
                {{--@foreach($date->tags as $tag)--}}
                {{--<span><a href="{{"/tags/".$tag->id}}">{{$tag->name}}</a></span>--}}
                {{--@endforeach--}}
            </article>
        @endforeach
    </div>
    <span class="">
        <a href="{{ route('commandes.create',['date' => $panier->id]) }}" class="btn btn-light text-center w-25">Valider</a>
    </span>
@stop
