@extends ('layouts.master')

@section('title')
    <title>Update date</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/dates">Home</a></li>
        <li><a href="/dates/create">Create</a></li>
        <li><a href="/dates/2/edit">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    {!! Form::model($date,['method' => 'put', 'route' => ['dates.update',$date->id]])!!}
    <div class="col-lg-2">
        <div class="form-group">
            {{Form::label('date', 'Date :')}}
            {{Form::date('date',date('Y-m-d', strtotime($date->date)))}}
        </div>
        <div class="form-group">
            {{Form::label('prix', 'prix :')}}
            {{Form::number('prix', null)}}
        </div>
        <div class="form-group">
            {{Form::label('concert_id', 'Concert :')}}
            {{Form::select('concert_id', $concert,null)}}
        </div>
        <div class="form-group">
            {{Form::submit('Envoyer')}}
        </div>
    </div>
    {!! Form::close() !!}
    <div class="row">
        <article class="col-sm-3">

        </article>
    </div>
@stop
