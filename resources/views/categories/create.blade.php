@extends ('layouts.master')

@section('title')
    <title>Create categorie</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/categories">Home</a></li>
        <li><a href="/categories/edit/1">Update</a> </li>
        <li><a href="/categories">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    {!! Form::open(['route' => 'categories.store']) !!}
    <div class="col-lg-2">
        <div class="form-group">
            {{Form::label('name', 'Name :')}}
            {{Form::text('name', 'Name')}}
        </div>
        <div class="form-group">
            {{Form::label('description', 'Description :')}}
            {{Form::textarea('description', 'Description')}}
        </div>
        <div class="form-group">
            {{Form::submit('Envoyer')}}
        </div>
    </div>
    {!! Form::close() !!}
    <div class="row">
        <article class="col-sm-3">

        </article>
    </div>
@stop
