@extends ('layouts.master')

@section('title')
    <title>View categorie {{$categorie->id}}</title>

@stop

@section('header')
    <ul class="nav navbar-dark">
        <li><a href="/categories">Home</a></li>
        <li><a href="{{"/categories/".$categorie->id."/edit"}}">Edit</a></li>
        <li><a href="/categories">Contact</a></li>
        <li><a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
             document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
@stop
@section('content')
    <div class="row">
        <article class="col-sm-6 text-center border border-secondary rounded">
            <p>Voici la catégorie {{$categorie->name}}</p>
            <a href="{{"/categories/".$categorie->id}}">{{$categorie->name}}</a>
            <p> {{$categorie->description}}</p>
        </article>
    </div>
@stop
