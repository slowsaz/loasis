@extends ('layouts.master')

@section('title')
    <title>Home</title>
@stop

@section('header')

@stop
@section('content')
    <h1>Mes catégories</h1>
    <div class="row">
        @foreach($categories as $categorie)

                <div class="col-lg-4 mb-4 text-dark">
                    <div class="card h-100">
                        <h4 class="card-header">{{$categorie->name}}</h4>
                        <div class="card-body">
                            <p class="card-text">{{$categorie->biography}}</p>
                        </div>
                        <div class="card-footer">
                            <a href="{{route('concertsCategories',['categorie'=>$categorie->id])}}" class="btn btn-dark">Learn More</a>
                        </div>
                    </div>
                </div>


            <article class="col-sm-6 text-center border border-secondary rounded">
                <a href="{{"/categories/".$categorie->id}}">{{$categorie->name}}</a>
                <span><p>{{$categorie->biography}}</p></span>
                <a href="{{ route('categories.edit',['categorie' => $categorie->id]) }}" class="btn btn-secondary text-center w-25">Editer</a>
                <form action="{{ route('categories.destroy', ['id' => $categorie->id]) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger text-center w-25">DELETE</button>
                    </div>
                </form>
            </article>
        @endforeach
    </div>

@stop
