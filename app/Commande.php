<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    protected $fillable = ['id','paiement','status','prixTotal','user_id'];

    public function dates(){
        return $this->belongsToMany('App\Date');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
