<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categorie;

class Concert extends Model
{
    protected $fillable=['name','description','duree', 'categorie_id'];

    public function artistes(){
        return $this->belongsToMany('App\Artiste');
    }

    public function categorie(){
        return $this->belongsTo('App\Categorie');
    }

    public function concerts(){
        return $this->hasMany('App\Concert');
    }

    public function dates(){
        return $this->belongsToMany('App\Dates');
    }
}
