<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreArtiste extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:artistes|string|max:191',
            'logo'=>'required',
            'biography' => 'required|string|max:191',
            'instagram'=>'string|max:191',
            'youtube'=>'string|max:191',
            'bandcamp'=>'string|max:191',
            'siteweb'=>'string|max:191',
            //
        ];
    }

    public function messages()
    {
        return [
            'name.unique' => 'L\'artiste ne pas être présent dans la base de données',
            'name.required'=>'Le nom de l\'artiste doit être saisi',
            'biography.required'=>'La biographie de l\'artiste doit être saisie',
            'logo.required'  => 'L\'artiste doit avoir une image',
        ];
    }

}
