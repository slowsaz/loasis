<?php

namespace App\Http\Controllers;

use App\Artiste;
use App\Concert;
use App\Http\Requests\StoreArtiste;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ArtisteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $artistes=Artiste::all();
        $artistes=Artiste::with('concerts','tags')->get();
        $artistes = Artiste::paginate(5);
        return view('/artistes/index',['artistes'=> $artistes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $concerts = Concert::pluck('name','id');
        $tags = Tag::pluck('name','id');
        return view('artistes/create',compact('concerts','tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArtiste $request)
    {
        $artiste=Artiste::create($request->all());
        $artiste->tags()->sync($request->tags);
        $artiste->concerts()->sync($request->concerts);
        $artiste->logo = $artiste->id . '.' .
            $request->file('logo')->getClientOriginalExtension();
//        dd($picture);
        $request->file('logo')->move(
            base_path() . '/public/img/', $artiste->logo
        );
        $artiste->logo='/img/'.$artiste->logo;
//        $logo = $request->file('logo').$picture->store('logo ');
        $artiste->save();
//        $artistes=Artiste::with('concerts','tags')->get();

        return redirect()->route('artistes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Artiste  $artiste
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $artiste=Artiste::findOrFail($id);
        return view('/artistes/show',['artiste'=>$artiste]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Artiste  $artiste
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artiste=Artiste::findOrFail($id);
        $concerts = Concert::pluck('name','id');
        $tags = Tag::pluck('name','id');
        return view('artistes/update',compact('concerts','tags','artiste'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Artiste  $artiste
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $artiste=Artiste::findOrFail($id);
        $artiste->update($request->all());
        $artiste->tags()->sync($request->tags);
        $artiste->concerts()->sync($request->concerts);
        return redirect('/artistes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Artiste  $artiste
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $artiste = Artiste::find($id);
        $artiste->tags()->detach();
        $artiste->concerts()->detach();
        $artiste->delete();
        return redirect()->route('artistes.index');
    }

    public function indexByTag($id){
        $tag = Tag::find($id);
        $artistes=$tag->artistes;
        return view('/artistes/index',['artistes'=> $artistes]);
    }
}
