<?php

namespace App\Http\Controllers;

use App\Commande;
use App\Date;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommandeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commandes=Commande::with('user')->get();
        $commandes=Commande::paginate(5);
        return view('/commandes/index',['commandes'=> $commandes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::pluck('name', 'id');
        $user = Auth::id();
//        $prix = Date::where(User::find($user));
//        dd($user = User::find($id)->dates);
        return view('commandes/create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::id();
        $commande = Commande::create($request->all());
        $panier = User::find(Auth::id());
        $commande->save();
        $panier->dates()->detach();
        return redirect()->route('commandesUsers',compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $commande=Commande::findOrFail($id);
        return view('/commandes/show',['commande'=>$commande]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $commande=Commande::findOrFail($id);
        $user = User::pluck('name', 'id');
        return view('commandes/update',compact('commande','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $commande=Commande::findOrFail($id);
        $commande->update($request->all());
//        $commande->user()->attach($request->user());
        return redirect('/commandes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $commande = Commande::find($id);
        $commande->delete();
        $user = Auth::id();
        return redirect()->route('commandesUsers',compact('user'));
    }

    public function ligneCommande(){


        $ligneCmd = User::find(Auth::id());
        $ligneCmd->dates()->detach();
        $user = Auth::id();
        return redirect()->route('commandesUsers',compact('user'));

    }

    public function indexByUser($id){
        $user= User::findOrFail($id);
        $commandes=Commande::where('user_id','=',$user->id)->get();
        return view('/commandes/user',['commandes'=> $commandes]);
    }
}
