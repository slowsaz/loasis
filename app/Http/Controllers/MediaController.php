<?php

namespace App\Http\Controllers;

use App\Concert;
use App\Media;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medias=Media::with('concert')->get();
        $medias=Media::paginate(5);
        return view('/medias/index',['medias'=> $medias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $concerts = Concert::pluck('name', 'id');
        return view('medias/create',compact('concerts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $media=Media::create($request->all());
        $medias=Media::with('concerts')->get();
        return redirect()->route('medias.index',['medias'=>$medias]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $media=Media::findOrFail($id);
        return view('/medias/show',['media'=>$media]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $media=Media::findOrFail($id);
        return view('/medias/update',['media'=>$media]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $media=Media::findOrFail($id);
        $media->update($request->all());
        return redirect('/medias');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media= Media::find($id);
        $media->delete();
        return redirect()->route('medias.index');
    }

    public function indexByConcert($id){
        $concert= Concert::findOrFail($id);
        $medias=Media::where('concert_id','=',$concert->id)->get();
        return view('/medias/index',['medias'=> $medias]);
    }
}
