<?php

namespace App\Http\Controllers;

use App\Concert;
use App\Categorie;
use App\Artiste;
use Illuminate\Http\Request;

class ConcertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $concerts=Concert::all();
        $concerts=Concert::with('artistes','categorie')->get();
        $concerts=Concert::paginate(5);
        return view('/concerts/index',['concerts'=> $concerts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Categorie::pluck('name', 'id');
        $artistes = Artiste::pluck('name', 'id');
        return view('concerts/create',compact('categories','artistes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $concerts=Concert::create($request->all());
        $concerts->artistes()->attach($request->artistes);
        $concerts->save();
//        $concerts=Concert::with('categorie')->get();
        return redirect()->route('concerts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Concert  $concert
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $concert=Concert::findOrFail($id);
        return view('/concerts/show',['concert'=>$concert]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Concert  $concert
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $concert=Concert::findOrFail($id);
        $categories = Categorie::pluck('name', 'id');
        $artistes = Artiste::pluck('name', 'id');
        return view('concerts/update',compact('categories','artistes','concert'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Concert  $concert
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $concert=Concert::findOrFail($id);
        $concert->update($request->all());
        $concert->artistes()->attach($request->artistes);
        return redirect('/concerts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Concert  $concert
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $concert = Concert::find($id);
        $concert->artistes()->detach();
        $concert->delete();
        return redirect()->route('concerts.index');
    }

    public function indexByCategory($id){
        $categorie = Categorie::findOrFail($id);
        $concerts=Concert::where('categorie_id','=',$categorie->id)->get();
        return view('/concerts/index',['concerts'=> $concerts]);
    }
}
