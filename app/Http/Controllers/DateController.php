<?php

namespace App\Http\Controllers;

use App\Commande;
use App\Concert;
use App\Date;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function Sodium\increment;

class DateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dates=Date::with('concert')->get();
        $dates=Date::paginate(5);
        return view('/dates/index',['dates'=> $dates]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $concert = Concert::pluck('name', 'id');
//        $users = User::pluck('name', 'id');
//        $commandes = Commande::pluck('name','id');
        return view('dates/create',compact('concert'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date=Date::create($request->all());
        $date->concert()->sync($request->concert);
//        $date->users()->sync($request->users);
//        $date->commandes()->sync($request->commandes);
        $date->save();
        return redirect()->route('dates.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Date  $date
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $date=Date::findOrFail($id);
        return view('/dates/show',['date'=>$date]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Date  $date
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $date=Date::findOrFail($id);
        $concert = Concert::pluck('name','id');
//        $users = User::pluck('name','id');
        return view('dates/update',compact('date','concert'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Date  $date
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $date=Date::findOrFail($id);
        $date->update($request->all());
        $date->concert()->sync($request->concert);
        return redirect('/dates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Date  $date
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dates = Date::find($id);
        $dates->delete();
        return redirect()->route('dates.index');
    }

    public function ajoutPanier($id){
        $date = Date::findOrFail($id);
//        dd($request->all());
//        dd($date->users);
        $date->save();
        $concert = Concert::pluck('name','id');
//        $date->users()->attach(Auth::id(),['quantite'=> +1]);
        $date->users()->sync(array(Auth::id()=>array('quantite'=> 1,'prix'=>$date->prix)));

        return redirect()->route('dates.index',compact('concert'));
    }

    public function indexPanier($id){
        $paniers = User::find($id)->dates;
//        dd($paniers);
        if($paniers->isEmpty()){
            return redirect()->route('dates.index');
        }
        $concert = Concert::pluck('name','id');
//        dd($paniers);
        return view ('dates/panier',compact('paniers','concert'));

    }

}
