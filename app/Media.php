<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $fillable=['name','type','link', 'concert_id'];

    public $table = "medias";

    public function concert(){
        return $this->belongsTo('App\Concert');
    }
}
