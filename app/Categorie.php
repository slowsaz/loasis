<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Concert;

class Categorie extends Model
{
    public function categories(){
        return $this->hasMany('App\Concert');
    }
}
