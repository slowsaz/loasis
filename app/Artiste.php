<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Concert;

class Artiste extends Model
{
    protected $fillable=['name','biography','logo','instragram','youtube','bandcamp','siteweb'];

    public function concerts(){
        return $this->belongsToMany('App\Concert');
    }

    public function tags(){
        return $this->belongsToMany('App\Tag');
    }
}
