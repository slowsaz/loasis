<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    protected $fillable=['date','prix','concert_id','user_id','quantite','prix'];

    public function concert(){
        return $this->belongsTo('App\Concert');
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function commandes(){
        return $this->belongsToMany('App\Commande');
    }
}
